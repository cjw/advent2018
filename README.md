# advent2018

Wrangling some PL/pgSQL for this set of advent problems.
I'm trying to solve things mostly _in SQL_ but I'm using the _PL_ features when the going gets hairy.

I want to learn PL/pgSQL a little better because it seems to me that, despite the prevailing winds of opinion these days, that putting some logic in the database is a good thing.
I can almost smell the [tar pit](http://shaffner.us/cs/papers/tarpit.pdf) from here.