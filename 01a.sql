create table frequencies (
       num decimal(10, 0)
);

\copy frequencies (num) from '01.txt';

select sum(num) from frequencies;
