require 'test/unit'

def n_of_any(str, n)
  len = str.length
  count = 1

  for i in 0...len do
    for j in 0...len do
      next if i == j
      count += 1 if str[i] == str[j]
    end

    return true if count == n
    count = 1
  end
  return false
end

class DummyTest2 < Test::Unit::TestCase
  def test_1
    assert(!n_of_any("abcdef", 2), "abcdef")
  end
  def test_2
    assert(n_of_any("bababc", 2), "bababc")
  end
  def test_3
    assert(n_of_any("abbcde", 2), "abbcde")
  end
  def test_4
    assert(!n_of_any("abcccd", 2), "abcccd")
  end
  def test_5
    assert(n_of_any("aabcdd", 2), "aabcdd")
  end
  def test_6
    assert(n_of_any("abcdee", 2), "abcdee")
  end
  def test_7
    assert(!n_of_any("ababab", 2), "ababab")
  end
end

class DummyTest3 < Test::Unit::TestCase
  def test_1
    assert(!n_of_any("abcdef", 3), "abcdef")
  end
  def test_2
    assert(n_of_any("bababc", 3), "bababc")
  end
  def test_3
    assert(!n_of_any("abbcde", 3), "abbcde")
  end
  def test_4
    assert(n_of_any("abcccd", 3), "abcccd")
  end
  def test_5
    assert(!n_of_any("aabcdd", 3), "aabcdd")
  end
  def test_6
    assert(!n_of_any("abcdee", 3), "abcdee")
  end
  def test_7
    assert(n_of_any("ababab", 3), "ababab")
  end
end