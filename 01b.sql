drop table if exists already_seen;
create table already_seen (
    n integer unique
);

insert into already_seen (n) values (0);

-- Repeatedly loop through frequencies (from part 1), inserting each into
-- already_seen (with a unique constraint). Yup, the answer is printed out as an
-- error message when run() eventually tries to insert a duplicate ;)
drop function if exists run;
create function run() returns integer as
$$
declare
    i integer := 0;
    total integer := 0;
begin
    loop
        total = total + (
            with numbered_frequencies as (
                select num, row_number() over () from frequencies
            )
            select num from numbered_frequencies
            where row_number = (i % 1000) + 1
        );
        insert into already_seen (n) values (total);
        i = i + 1;
    end loop;
    return 0;
end;
$$ language plpgsql;

select run();